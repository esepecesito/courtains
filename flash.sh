# port install openocd +usb_blaster
#openocd -f interface/altera-usb-blaster.cfg -f target/stm32f1x.cfg -c init -c "reset halt" -c "flash write_image erase build/Blinky.bin 0x08000000" -c "reset run" -c "exit"

##[+]ftdi: Enable building OpenOCD's built-in FTDI driver.
#     * conflicts with ft2232_ftd2xx

openocd -f interface/ftdi/openocd-usb-hs.cfg -f target/stm32f1x.cfg -c init -c "reset halt" -c "flash write_image erase build/FRTOS_Blinky.bin 0x08000000" -c "reset run" -c "exit"

