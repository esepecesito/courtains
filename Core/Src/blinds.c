
enum blind_states {
	blind_idle=0, 
	blind_up=1, 
	blind_down=2, 
	blind_up_auto=3, 
	blind_down_auto=4
};

struct blind
{
int timer, delay;
enum blind_states state;
};

struct blind blinds[2];

void blind_init()
{
blinds[0].state=blind_idle;
blinds[0].timer=1000;
blinds[0].delay=6;
blinds[1].state=blind_idle;
blinds[1].timer=2000;
blinds[1].delay=6;
}

void blind_stop(int blind)
{
blinds[blind].state=blind_idle;
blinds[blind].timer=2000;
blinds[blind].delay=6;
if (blind==0){
	HAL_GPIO_WritePin(OUT1_GPIO_Port, OUT1_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(OUT2_GPIO_Port, OUT2_Pin, GPIO_PIN_SET);
} else {
	HAL_GPIO_WritePin(OUT3_GPIO_Port, OUT3_Pin, GPIO_PIN_SET);
	HAL_GPIO_WritePin(OUT4_GPIO_Port, OUT4_Pin, GPIO_PIN_SET);
}
}

void blind_go_down(int blind)
{
blinds[blind].state=blind_down;
if (blind == 0)
	blinds[blind].timer=2000;
else
	blinds[blind].timer=1000;
blinds[blind].delay=6;
}

void blind_go_up(int blind)
{
blinds[blind].state=blind_up;
if (blind == 0)
	blinds[blind].timer=2000;
else
	blinds[blind].timer=1000;
blinds[blind].delay=6;
}

void blind_timer_tick()
{

if (blinds[0].state == blind_up){
	HAL_GPIO_WritePin(OUT1_GPIO_Port, OUT1_Pin, GPIO_PIN_RESET);
	if (blinds[0].delay<=0){
		HAL_GPIO_WritePin(OUT2_GPIO_Port, OUT2_Pin, GPIO_PIN_RESET);
		blinds[0].timer--;
	} else {
		blinds[0].delay--;
	}
	if (blinds[0].timer<=0) {
		blinds[0].state=blind_idle;
		HAL_GPIO_WritePin(OUT1_GPIO_Port, OUT1_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(OUT2_GPIO_Port, OUT2_Pin, GPIO_PIN_SET);
	}
}

if (blinds[0].state == blind_down){
	HAL_GPIO_WritePin(OUT1_GPIO_Port, OUT1_Pin, GPIO_PIN_SET);
	if (blinds[0].delay<=0){
		HAL_GPIO_WritePin(OUT2_GPIO_Port, OUT2_Pin, GPIO_PIN_RESET);
		blinds[0].timer--;
	} else {
		blinds[0].delay--;
	}
	if (blinds[0].timer<=0) {
		blinds[0].state=blind_idle;
		HAL_GPIO_WritePin(OUT1_GPIO_Port, OUT1_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(OUT2_GPIO_Port, OUT2_Pin, GPIO_PIN_SET);
	}
}

if (blinds[1].state == blind_up){
	HAL_GPIO_WritePin(OUT3_GPIO_Port, OUT3_Pin, GPIO_PIN_RESET);
	if (blinds[1].delay<=0){
		HAL_GPIO_WritePin(OUT4_GPIO_Port, OUT4_Pin, GPIO_PIN_RESET);
		blinds[1].timer--;
	} else {
		blinds[1].delay--;
	}
	if (blinds[1].timer<=0) {
		blinds[1].state=blind_idle;
		HAL_GPIO_WritePin(OUT3_GPIO_Port, OUT3_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(OUT4_GPIO_Port, OUT4_Pin, GPIO_PIN_SET);
	}
} 

if (blinds[1].state == blind_down){
	HAL_GPIO_WritePin(OUT3_GPIO_Port, OUT3_Pin, GPIO_PIN_SET);
	if (blinds[1].delay<=0){
		HAL_GPIO_WritePin(OUT4_GPIO_Port, OUT4_Pin, GPIO_PIN_RESET);
		blinds[1].timer--;
	} else {
		blinds[1].delay--;
	}
	if (blinds[1].timer<=0) {
		blinds[1].state=blind_idle;
		HAL_GPIO_WritePin(OUT3_GPIO_Port, OUT3_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(OUT4_GPIO_Port, OUT4_Pin, GPIO_PIN_SET);
	}
}
}
