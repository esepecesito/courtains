#include "keypad.h"

void do_nothing(void)
{}

void toggle_out1 (void)
{ HAL_GPIO_TogglePin(OUT1_GPIO_Port, OUT1_Pin); }

void toggle_out2 (void)
{ HAL_GPIO_TogglePin(OUT2_GPIO_Port, OUT2_Pin); }

void toggle_out3 (void)
{ HAL_GPIO_TogglePin(OUT3_GPIO_Port, OUT3_Pin); }

void toggle_out4 (void)
{ HAL_GPIO_TogglePin(OUT4_GPIO_Port, OUT4_Pin); }

void toggle_out5 (void)
{ HAL_GPIO_TogglePin(OUT5_GPIO_Port, OUT5_Pin); }

void toggle_out6 (void)
{ HAL_GPIO_TogglePin(OUT6_GPIO_Port, OUT6_Pin); }

void toggle_out7 (void)
{ HAL_GPIO_TogglePin(OUT7_GPIO_Port, OUT7_Pin); }

void toggle_out8 (void)
{ HAL_GPIO_TogglePin(OUT8_GPIO_Port, OUT8_Pin); }

int blind0_full, blind1_full;

void blind0_up (void)
{ blind0_full=0; blind_go_up(0);}

void blind0_up_full (void)
{ blind0_full=1; blind_go_up(0);}

void blind0_down (void)
{ blind0_full=0; blind_go_down(0);}

void blind0_down_full (void)
{ blind0_full=1; blind_go_down(0);}

void blind0_stop (void)
{ if(!blind0_full) blind_stop(0); blind0_full=0;}

void blind1_up (void)
{ blind1_full=0; blind_go_up(1);}

void blind1_up_full (void)
{ blind1_full=1; blind_go_up(1);}

void blind1_down (void)
{ blind1_full=0; blind_go_down(1);}

void blind1_down_full (void)
{ blind1_full=1; blind_go_down(1);}

void blind1_stop (void)
{ if(!blind1_full) blind_stop(1); blind1_full=0;}

void KeyPad_init (void)
{
lastInput=0;
key=0;
lastRelease=0;
repetitions=KEYPAD_DEBOUNCE_TICKS;
releaseTicks=0;
key_state=key_idle;

keys[0].press=do_nothing;
keys[0].release=do_nothing;
keys[0].hold=do_nothing;
keys[0].doubleTap=do_nothing;

keys[1].press=toggle_out5;
keys[1].release=do_nothing;
keys[1].hold=do_nothing;
keys[1].doubleTap=toggle_out5;

keys[2].press=toggle_out6;
keys[2].release=do_nothing;
keys[2].hold=do_nothing;
keys[2].doubleTap=toggle_out6;

keys[3].press=blind0_stop;
keys[3].release=blind0_stop;
keys[3].hold=blind0_up;
keys[3].doubleTap=blind0_up_full;

keys[4].press=blind1_stop;
keys[4].release=blind1_stop;
keys[4].hold=blind1_up;
keys[4].doubleTap=blind1_up_full;

keys[5].press=blind0_stop;
keys[5].release=blind0_stop;
keys[5].hold=blind0_down;
keys[5].doubleTap=blind0_down_full;

keys[6].press=blind1_stop;
keys[6].release=blind1_stop;
keys[6].hold=blind1_down;
keys[6].doubleTap=blind1_down_full;

keys[7].press=toggle_out7;
keys[7].release=do_nothing;
keys[7].hold=toggle_out1;
keys[7].doubleTap=toggle_out2;

keys[8].press=toggle_out8;
keys[8].release=do_nothing;
keys[8].hold=do_nothing;
keys[8].doubleTap=toggle_out8;

}
    
void KeyPad_idle (int input)
{
  if (input == lastInput)
    {
      if (repetitions <= KEYPAD_DEBOUNCE_TICKS)
	repetitions++;
    } 
  else 
    {
      repetitions=0;
    }
  if (repetitions == KEYPAD_DEBOUNCE_TICKS) // only if equal!
    {
      if (input == 0)
	{
	  keys[key].release();
	  releaseTicks=KEYPAD_DOUBLE_TICKS;
	}
      else
	{
	  if ((input == lastRelease) && 
	      (releaseTicks > 0)) // Double-tap?
	    {
	      keys[input].doubleTap();
	    }
	  else
	    {
	      if (key != 0)
		keys[key].release();
	      keys[input].press();
	      key_state=key_pressed;
	    }
	}
      lastRelease=key;
      key=input;
    }

}
  
void KeyPad_pressed(int input)
{
  if (input == key)
    {
      if (lastInput == key)
	{
	  repetitions++;
	  if (repetitions == KEYPAD_HOLD_TICKS)
	    {
	      keys[input].hold();
	    }
	}
      else
	{
	  repetitions=0;
	}
    }
  else
    {
      if (lastInput!= key)
	{
	  repetitions++;
	  if (repetitions == KEYPAD_DEBOUNCE_TICKS)
	    {
	      key_state=key_idle;
	      repetitions=0;
	    }
	}
      else
	{
	  repetitions=0;
	}
    }
}
  
void KeyPad_event (int input)
{
  switch (key_state) {
  case key_idle:
    KeyPad_idle(input);
    break;
  case key_pressed:
    KeyPad_pressed(input);
    break;
  }
  lastInput=input;
  if (releaseTicks > 0)
    releaseTicks--;
}
