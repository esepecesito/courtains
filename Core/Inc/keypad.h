#ifndef KEYPAD_HPP_INCLUDED
#define KEYPAD_HPP_INCLUDED

#define KEYPAD_DEBOUNCE_TICKS 1
#define KEYPAD_DOUBLE_TICKS 5 
#define KEYPAD_HOLD_TICKS 15

struct KeyActions
{
  void (*press)(void);
  void (*release)(void);
  void (*hold)(void);
  void (*doubleTap)(void);
};

enum key_states { key_idle=0, key_pressed=1 } key_state;
int lastInput, key, lastRelease;
int repetitions, releaseTicks;
struct KeyActions keys[9];

#endif // KEYPAD_HPP_INCLUDED
